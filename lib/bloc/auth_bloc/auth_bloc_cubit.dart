import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/database_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocLoginState());

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    SharedPreferences? sharedPreferences =
        await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void loginUser(String email, String password) async {
    SharedPreferences? sharedPreferences =
        await SharedPreferences.getInstance();

    var db = DatabaseHelper();
    User? user = await db.selectUser(email, password);
    if (user == null) {
      emit(AuthBlocErrorState("Login gagal, periksa kembali inputan anda"));
    } else {
      await sharedPreferences.setBool("is_logged_in", true);
      var data = user.toJson();
      sharedPreferences.setString("user_value", data.toString());
      var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.mobile) {
        emit(AuthBlocLoadedState(data));
      } else if (connectivityResult == ConnectivityResult.wifi) {
        emit(AuthBlocLoadedState(data));
      } else {
        emit(AuthBlocErrorState("Anda tidak tersambung ke internet"));
      }
    }
  }

  void logoutUser() async {
    SharedPreferences? sharedPreferences =
        await SharedPreferences.getInstance();
    await sharedPreferences.setBool("is_logged_in", false);
    emit(AuthBlocLoginState());
  }

  void registerUser(User registerUser) async {
    SharedPreferences? sharedPreferences =
        await SharedPreferences.getInstance();
    User user = User(
      email: registerUser.email,
      userName: registerUser.userName,
      password: registerUser.password,
    );
    var db = DatabaseHelper();
    await db.saveUser(user);
    await sharedPreferences.setBool("is_logged_in", true);
    var data = user.toJson();
    sharedPreferences.setString("user_value", data.toString());
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      emit(AuthBlocLoadedState(data));
    } else if (connectivityResult == ConnectivityResult.wifi) {
      emit(AuthBlocLoadedState(data));
    } else {
      emit(AuthBlocErrorState("Anda tidak tersambung ke internet"));
    }
  }
}
