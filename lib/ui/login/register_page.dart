import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _emailController = TextController();
  final _passwordController = TextController();
  final _confirmPasswordController = TextController();
  final _userNameController = TextController();
  final authBlocCubit = AuthBlocCubit();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  bool _isObscurePassword = true;

  @override
  void initState() {
    //authBlocCubit = BlocProvider.of<AuthBlocCubit>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        bloc: authBlocCubit,
        listener: (context, state) {
          if (state is AuthBlocLoadedState) {
            _showSnackbar("Register berhasil");
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (_) => BlocProvider(
                    create: (context) => HomeBlocCubit()..fetching_data(),
                    child: HomeBlocScreen(),
                  ),
                ),
                (Route<dynamic> route) => false);
          } else if (state is AuthBlocErrorState) {
            _showSnackbar(state.error);
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Selamat Datang',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                Text(
                  'Silahkan daftarkan akun anda terlebih dahulu',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 9,
                ),
                _registerForm(),
                SizedBox(
                  height: 50,
                ),
                BlocListener<AuthBlocCubit, AuthBlocState>(
                  bloc: authBlocCubit,
                  listener: (context, state) {
                    if (state is AuthBlocLoadingState) {
                      LoadingIndicator();
                    } else if (state is AuthBlocLoggedInState) {
                      _showSnackbar("Register Berhasil");
                    }
                  },
                  child: CustomButton(
                    text: 'Sign Up',
                    onPressed: () {
                      handleRegister();
                    },
                    height: 100,
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _registerForm() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            label: 'Email',
            hint: 'Fill your email here',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val) ? null : 'email is invalid';
            },
          ),
          CustomTextFormField(
            context: context,
            controller: _userNameController,
            label: 'Username',
            hint: 'Fill your User Name here',
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
          CustomTextFormField(
            context: context,
            label: 'Confirm Password',
            hint: 'confirm password',
            controller: _confirmPasswordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  void handleRegister() async {
    final String? _email = _emailController.value;
    final String? _password = _passwordController.value;
    final String? _userName = _userNameController.value;
    final String? _confirmPassword = _confirmPasswordController.value;
    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null &&
        _confirmPassword != null &&
        _userName != null) {
      if (_password != _confirmPassword) {
        _showSnackbar("Mismatch password and and confirmed password");
      } else {
        User user = User(
          email: _email,
          userName: _userName,
          password: _password,
        );
        authBlocCubit.registerUser(user);
      }
    }
  }

  void _showSnackbar(String text) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(text)));
  }
}
