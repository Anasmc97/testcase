import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/series_card.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/utils/theme.dart';

class MovieDetailPage extends StatefulWidget {
  final Data? data;
  MovieDetailPage(
    this.data,
  );
  @override
  _MovieDetailPageState createState() => _MovieDetailPageState();
}

class _MovieDetailPageState extends State<MovieDetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        Container(
          color: accentColor1,
        ),
        SafeArea(
          child: Container(
            color: Colors.white,
          ),
        ),
        ListView(
          children: [
            Column(
              children: [
                //BACKDROP
                _backDrop(),
                //TITLE
                _titleYear(),
                //series
                _series()
              ],
            )
          ],
        )
      ],
    ));
  }

  Widget _backDrop() {
    return Stack(
      children: [
        Stack(
          children: <Widget>[
            Container(
              height: 270,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(widget.data!.i!.imageUrl!),
                      fit: BoxFit.cover)),
            ),
            Container(
              height: 271,
              width: double.infinity,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: const Alignment(0, 1),
                      end: const Alignment(0, 0.06),
                      colors: [Colors.white, Colors.white.withOpacity(0)])),
            )
          ],
        ),
        Container(
          margin: const EdgeInsets.only(top: 20, left: defaultMargin),
          padding: const EdgeInsets.all(1),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: Colors.black.withOpacity(0.04)),
          child: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }

  Widget _titleYear() {
    return Column(
      children: [
        Container(
          margin:
              const EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
          child: Text(
            widget.data!.l ?? "",
            textAlign: TextAlign.center,
            style: blackTextFont.copyWith(fontSize: 24),
          ),
        ),
        Text(
          "${widget.data!.year}",
          style:
              greyTextFont.copyWith(fontSize: 24, fontWeight: FontWeight.w400),
        ),
      ],
    );
  }

  Widget _series() {
    return Column(
      children: [
        Align(
          alignment: Alignment.topLeft,
          child: Container(
              margin: const EdgeInsets.only(
                  top: 20, left: defaultMargin, bottom: 12),
              child: Text(
                "Series",
                style: blackTextFont.copyWith(fontSize: 20),
              )),
        ),
        SizedBox(
          height: 150,
          //menampilkan series
          child: (widget.data!.series != null)
              ? ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: widget.data!.series!.length,
                  itemBuilder: (_, index) => Container(
                      margin: EdgeInsets.only(
                          left: (index == 0) ? defaultMargin : 0,
                          right: (index == widget.data!.series!.length - 1)
                              ? defaultMargin
                              : 16),
                      child: SeriesCard(widget.data!.series![index])))
              : Center(
                  child: Text("No series",
                      style: blackTextFont.copyWith(
                          fontSize: 20, fontWeight: FontWeight.w400))),
        )
      ],
    );
  }
}
