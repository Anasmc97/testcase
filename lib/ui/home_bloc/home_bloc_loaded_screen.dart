import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:majootestcase/ui/movie_detail.dart';
import 'package:majootestcase/utils/theme.dart';

class HomeBlocLoadedScreen extends StatefulWidget {
  final List<Data>? data;

  const HomeBlocLoadedScreen({Key? key, this.data}) : super(key: key);

  @override
  State<HomeBlocLoadedScreen> createState() => _HomeBlocLoadedScreenState();
}

class _HomeBlocLoadedScreenState extends State<HomeBlocLoadedScreen> {
  final authBlocCubit = AuthBlocCubit();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: accentColor1,
          title: Text(
            "Movies",
            style: whiteTextFont.copyWith(
                fontSize: 20, fontWeight: FontWeight.w600),
          ),
          actions: [
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    authBlocCubit.logoutUser();
                  },
                  child: Icon(
                    Icons.logout_outlined,
                    size: 26.0,
                  ),
                )),
          ],
        ),
        body: BlocListener<AuthBlocCubit, AuthBlocState>(
          bloc: authBlocCubit,
          listener: (context, state) {
            if (state is AuthBlocLoadingState) {
              LoadingIndicator();
            } else if (state is AuthBlocLoginState) {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => LoginPage()));
            }
          },
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ListView.builder(
                  itemCount: widget.data!.length,
                  physics: const NeverScrollableScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) =>
                                  MovieDetailPage(widget.data![index])));
                        },
                        child: movieItemWidget(
                            widget.data![index], index, context));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget movieItemWidget(Data data, int index, BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: index == 0 ? 10 : 20, left: 20),
      child: Row(
        //poster dan detailmovie
        children: <Widget>[
          Container(
            //gambar poster
            width: 70,
            height: 90,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                image: DecorationImage(
                    image: NetworkImage(data.i!.imageUrl!), fit: BoxFit.cover)),
          ),
          const SizedBox(width: 16),
          SizedBox(
            width:
                MediaQuery.of(context).size.width - 2 * defaultMargin - 70 - 16,
            child: Column(
              //detail movie
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  data.l ?? "",
                  style: blackTextFont.copyWith(fontSize: 18),
                  maxLines: 2,
                  overflow: TextOverflow.clip,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget refreshWidget(BuildContext context) {
    return Column(
      children: [
        Icon(Icons.wifi_off),
        ElevatedButton(onPressed: () {}, child: Text("Refresh Button"))
      ],
    );
  }
}
