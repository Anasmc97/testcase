class MovieDetail {
  final String? title;
  final String? poster;
  final String? year;
  final List<String?>? listSeriesTitle;
  final List<String?>? listSeriesPoster;

  MovieDetail(
      {this.title,
      this.poster,
      this.year,
      this.listSeriesTitle,
      this.listSeriesPoster});

  MovieDetail.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        poster = json['poster'],
        year = json['year'],
        listSeriesTitle = json['listSeriesTitle'],
        listSeriesPoster = json['listSeriesPoster'];

  Map<String, dynamic> toJson() => {
        'title': title,
        'poster': poster,
        'year': year,
        'listSeriesTitle': listSeriesTitle,
        'listSeriesPoster': listSeriesPoster
      };
}
