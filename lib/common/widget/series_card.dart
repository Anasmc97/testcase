import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/utils/theme.dart';

class SeriesCard extends StatelessWidget {
  final Series series;

  const SeriesCard(this.series, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 100,
          width: 90,
          decoration: BoxDecoration(
              color: Colors.blueGrey[100],
              borderRadius: BorderRadius.circular(8),
              image: DecorationImage(
                  image: NetworkImage(series.i!.imageUrl!), fit: BoxFit.cover)),
        ),
        Container(
          margin: const EdgeInsets.only(top: 6),
          width: 70,
          child: Text(
            series.l ?? "",
            textAlign: TextAlign.center,
            maxLines: 2,
            overflow: TextOverflow.clip,
            style: blackTextFont.copyWith(
                fontSize: 10, fontWeight: FontWeight.w400),
          ),
        ),
      ],
    );
  }
}
